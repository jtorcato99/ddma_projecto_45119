package com.projetofinal.josetorcato.projetofinal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MenuActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAuth firebaseAuth;
    private TextView textViewUserEmail;
    private Button buttonLogout, btn_quemSomos, btn_oqueFazemos, btn_Portfolio, btn_Servicos, btn_Contactos, btn_Informacoes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        firebaseAuth = FirebaseAuth.getInstance();
        if(firebaseAuth.getCurrentUser() == null){

            finish();

            startActivity(new Intent(this, LoginActivity.class));
        }
        FirebaseUser user = firebaseAuth.getCurrentUser();

        textViewUserEmail = findViewById(R.id.textViewUserEmail);
        buttonLogout = findViewById(R.id.buttonLogout);
        btn_quemSomos = findViewById(R.id.btn_quemSomos);
        btn_oqueFazemos = findViewById(R.id.btn_oqueFazemos);
        btn_Portfolio = findViewById(R.id.btn_Portfolio);
        btn_Servicos = findViewById(R.id.btn_Servicos);
        btn_Contactos = findViewById(R.id.btn_Contactos);
        btn_Informacoes = findViewById(R.id.btn_Informacoes);

        textViewUserEmail.setText("Bem-vindo " + "\n" +user.getEmail());

        buttonLogout.setOnClickListener(this);

        btn_quemSomos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MenuActivity.this, QuemSomosActivity.class);
                startActivity(myIntent);
            }
        });

        btn_oqueFazemos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MenuActivity.this, OQueFazemosActivity.class);
                startActivity(myIntent);
            }
        });

        btn_Portfolio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MenuActivity.this, PortfolioActivity.class);
                startActivity(myIntent);
            }
        });

        btn_Servicos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MenuActivity.this, ServicosActivity.class);
                startActivity(myIntent);
            }
        });

        btn_Contactos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MenuActivity.this, Contactos_Activity.class);
                startActivity(myIntent);
            }
        });

        btn_Informacoes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MenuActivity.this, InfoActivity.class);
                startActivity(myIntent);
            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view == buttonLogout){
            firebaseAuth.signOut();
            finish();
            startActivity(new Intent(this, IndexActivity.class));
        }
    }

}
