package com.projetofinal.josetorcato.projetofinal;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAuth firebaseAuth;
    private EditText Email;
    private EditText Password;
    private Button btn_register;
    private TextView textViewSignIn;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        firebaseAuth = FirebaseAuth.getInstance();
        Email = findViewById(R.id.Email);
        Password = findViewById(R.id.Password);
        btn_register = findViewById(R.id.btn_Register);
        progressDialog = new ProgressDialog(this);
        btn_register.setOnClickListener(this);
        textViewSignIn = findViewById(R.id.textViewSignIn);
        textViewSignIn.setOnClickListener(this);

    }

    private void registerUser(){

        String email = Email.getText().toString().trim();
        String password  = Password.getText().toString().trim();

        if(TextUtils.isEmpty(email)){
            Toast.makeText(this,"Por favor, insira um email!",Toast.LENGTH_LONG).show();
            return;
        }

        if(TextUtils.isEmpty(password)){
            Toast.makeText(this,"Por favor, insira uma password!",Toast.LENGTH_LONG).show();
            return;
        }

        progressDialog.setMessage("A resgistar, por favor aguarde...");
        progressDialog.show();

        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(RegisterActivity.this,"Registo efetuado com sucesso!",Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(RegisterActivity.this,"Este email já existe, por favor verifique!",Toast.LENGTH_LONG).show();
                        }
                        progressDialog.dismiss();
                    }
                });

    }

    @Override
    public void onClick(View view) {

        if(view == btn_register){
            registerUser();
        }

        if(view == textViewSignIn){
            startActivity(new Intent(this, LoginActivity.class));
        }

    }
}
