package com.projetofinal.josetorcato.projetofinal;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PortfolioActivity extends AppCompatActivity {

    Button bt_portoflio_photo;
    Button bt_portoflio_programacao;
    Button bt_portoflio_design;
    Button bt_portoflio_audiovisuais;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_portfolio);

        bt_portoflio_photo = findViewById(R.id.bt_portfolio_photo);
        bt_portoflio_programacao = findViewById(R.id.bt_portfolio_programacao);
        bt_portoflio_design = findViewById(R.id.bt_portfolio_design);
        bt_portoflio_audiovisuais = findViewById(R.id.bt_portfolio_audiovisuais);

        bt_portoflio_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("http://www.facebook.com/JoseTorcatoPhotography/"));
                startActivity(viewIntent);
            }
        });

        bt_portoflio_programacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("http://jtmedia.aesmprojetos.net/programacao.php"));
                startActivity(viewIntent);
            }
        });

        bt_portoflio_design.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("http://jtmedia.aesmprojetos.net/design.php"));
                startActivity(viewIntent);
            }
        });

        bt_portoflio_audiovisuais.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("http://jtmedia.aesmprojetos.net/audiovideo.php"));
                startActivity(viewIntent);
            }
        });

    }
}
