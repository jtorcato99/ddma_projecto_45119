package com.projetofinal.josetorcato.projetofinal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class IndexActivity extends AppCompatActivity {

    private Button btn_GoLogin;
    private Button btn_GoRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);

        btn_GoLogin = findViewById(R.id.btn_GoLogin);
        btn_GoRegister = findViewById(R.id.btn_GoRegister);

        btn_GoLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(IndexActivity.this, LoginActivity.class);
                startActivity(myIntent);
            }
        });

        btn_GoRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(IndexActivity.this, RegisterActivity.class);
                startActivity(myIntent);
            }
        });
    }
}
