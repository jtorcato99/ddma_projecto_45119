package com.projetofinal.josetorcato.projetofinal;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Contactos_Activity extends AppCompatActivity {

    public Button bt_Call, bt_emailsent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactos);

        bt_Call = findViewById(R.id.bt_call);
        bt_emailsent = findViewById(R.id.bt_emailsent);


        bt_Call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:+351964407733"));
                startActivity(callIntent);
            }
        });

        bt_emailsent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Contactos_Activity.this, EmailActivity.class);
                startActivity(myIntent);
            }
        });

    }
}
